**OurPlant X3 blue**
The axis system is made of aluminum and is the cheaper version compared to the Black Edition.

Like all micro assembly machines of OurPlant, the OurPlant X3 blue is based on a modular design. Thanks to a large number of processing modules, the X3 blue realises different technologies and complex processes.

The machine‘s working heads have an exceptional z stroke of 150 mm, which makes 3D assembly possible.

Advantages:
– Ideal for medium and large batch sizes
– low set-up thanks to real plug & play capability
– easy integration into a production line by using a transport system
– software-based control panel via touch screen

Technical information:
– Working heads with integrated Z-axis
– per portal one interface with 10 electrical connections (5x CAN, 5x Ethernet)
– control panel
– Software and control with integrated industrial PC

 

**Other variants of the OurPlant X3:**

**OurPlant X3 blue²**
It is possible to equip the OurPlant X3 blue with a second portal. The OurPlant X3 blue² has  two independently working Y-axes. Compared to a OurPlant X3 with on single Y-axis,  the double portal allows the use of twice as many working heads. The advantage: complex processes can be realised in parallel and thus the cycle time is reduced. The axis system is made of aluminum and is the cheaper version compared to the Black Edition.

**OurPlant X3 black**
The OurPlant X3 is also available in a light version. In the Black edition, the axes are made of carbon. The lower axis weight means that products can be processed faster on this system. Due to the positive material properties of the carbon, the precision of the machining heads also increases. The OurPlant X3 black uses a two-axis system (X, Y).

**OurPlant X3 black²**
The OurPlant X3 is also available in the light version with the OurPlant X3 black². Its axes are made of carbon. The lower axis weight helps to process the products faster on this system. Thanks to the carbon’s positive material the precision of the working heads also increases. The OurPlant X3 black² works with a second portal. Thus complex processes can be realised in parallel and thus the cycle time is reduced again.

 

**Extension:**

All variants of the OurPlant X3 can be ordered with an additional protective housing, which serves the laser protection (wavelength 940 nm – 1065 nm).

All variants of the OurPlant X3 are optionally offered with UL certification.