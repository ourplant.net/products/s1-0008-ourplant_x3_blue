Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s1-0008-ourplant_x3_blue).

| document | download options |
|:-------- | ----------------:|
| operating manual           |[de](https://gitlab.com/ourplant.net/products/s1-0008-ourplant_x3_blue/-/raw/main/01_operating_manual/S1-0008-0011_D1_BA_OurPlant%20X3.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s1-0008-ourplant_x3_blue/-/raw/main/02_assembly_drawing/s1-0008_D_ZNB_ourplant_x3_blue.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s1-0008-ourplant_x3_blue/-/raw/main/03_circuit_diagram/S1-0008_to_S1-0011_A_EPLAN_OurPlant%20X3.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s1-0008-ourplant_x3_blue/-/raw/main/04_maintenance_instructions/S1-0008-0011_E_WA_OurPlant%20X3.pdf)
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s1-0008-ourplant_x3_blue/-/raw/main/05_spare_parts/S1-0008_C_EVL_OurPlant%20X3%20.pdf), [en](https://gitlab.com/ourplant.net/products/s1-0008-ourplant_x3_blue/-/raw/main/05_spare_parts/S1-0008_C_SWP_OurPlantX3.pdf)|

